;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2022 Gerry Agbobada <git@gagbo.net>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gagbo packages emacs)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix memoization)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (gnu packages emacs)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26))

;; Taken from
;; https://github.com/flatwhatson/guix-channel/blob/master/flat/packages/emacs.scm
(define emacs-from-git
  (lambda* (emacs #:key pkg-name pkg-version pkg-revision git-repo git-commit checksum)
    (package
      (inherit emacs)
      (name pkg-name)
      (version (git-version pkg-version pkg-revision git-commit))
      (source
       (origin
         (inherit (package-source emacs))
         (method git-fetch)
         (uri (git-reference
               (url git-repo)
               (commit git-commit)))
         (sha256 (base32 checksum))
         (file-name (git-file-name pkg-name pkg-version))))
      (outputs
       '("out" "debug")))))

(define-public emacs-lsp
  (emacs-from-git
   emacs-next
   #:pkg-name "emacs-lsp"
   #:pkg-version "29.0.60"
   #:pkg-revision "3"
   #:git-repo "https://github.com/emacs-lsp/emacs.git"
   #:git-commit "87596d041094e398c4823f1b41a87690478cefca"
   #:checksum "1j9r8403z6b94xh3gkl5v5gd1kdnyfw12f6256w9r6jy4kn6d6nd"))
