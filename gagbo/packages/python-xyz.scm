;;; Gagbo Python packages --- Functional python package management
;;; Copyright © 2021 Gerry Agbobada <srht@gagbo.net>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gagbo packages python-xyz)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages time)
  #:use-module (gnu packages xml))


;;; ("xunitparser")
(define-public python-xunitparser
  (package
    (name "python-xunitparser")
    (version "1.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "xunitparser" version))
        (sha256
          (base32 "05amn9yik0mxg89iiprkb6lrmc7rlccgvwajrpyfi6zbp8mjdsgn"))))
    (build-system python-build-system)
    (home-page "http://git.p.engu.in/laurentb/xunitparser/")
    (synopsis "Read JUnit/XUnit XML files and map them to Python objects")
    (description "Read JUnit/XUnit XML files and map them to Python objects")
    (license #f)))

;;; ("woob")
(define-public python-woob
  (package
    (name "python-woob")
    (version "3.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "woob" version))
        (sha256
          (base32 "09hpxy5zhn2b8li0xjf3zd7s46lawb0315p5mdcsci3bj3s4v1j7"))))
    (build-system python-build-system)
    (arguments '(#:tests? #f))
    ;; (native-inputs
    ;;   `(("python-coverage" ,python-coverage)
    ;;     ("python-xunitparser" ,python-xunitparser)))
    (propagated-inputs
      `(("python-babel" ,python-babel)
        ("python-cssselect" ,python-cssselect)
        ("python-dateutil" ,python-dateutil)
        ("python-html2text" ,python-html2text)
        ("python-lxml" ,python-lxml)
        ("python-pillow" ,python-pillow)
        ("python-pyyaml" ,python-pyyaml)
        ("python-requests" ,python-requests)
        ("python-six" ,python-six)
        ("python-unidecode" ,python-unidecode)))
    (home-page "https://woob.tech/")
    (synopsis "Woob, Web Outside Of Browsers")
    (description "Woob, Web Outside Of Browsers")
    (license #f)))
