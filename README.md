# Personal Guix channel

> Note
>
> I lost the first key I used to sign commits, so the channel
> introduction has been modified.

To import it, use

```scheme
;; In ~/.config/guix/channels
(cons* (channel
        (name 'gagbo)
        (url "https://git.sr.ht/~gagbo/guix-channel")
        (branch "main")
        (introduction
         (make-channel-introduction
          "a541140d767987909881094725903bca8ebd6410"
          (openpgp-fingerprint
           "3B9E CFDB 806B 65C0 DE68  E044 7EB3 0AF8 BE2A EF14"))))
       %default-channels)
```
